# syntax=docker/dockerfile:experimental
FROM maven:3-jdk-8-alpine AS builder

WORKDIR /usr/src/app
COPY settings.xml /usr/share/maven/conf/settings.xml
COPY ./pom.xml .
COPY ./src src
RUN --mount=type=cache,target=/root/.m2 mvn install -DskipTests

FROM openjdk:8-jre-alpine
COPY --from=builder /usr/src/app/target/wxDemo.jar wxDemo.jar
COPY ./src/main/resources/application.properties /config/application.properties
ENV PORT 8080
EXPOSE $PORT
CMD [ "java", "-jar", "wxDemo.jar" ]
