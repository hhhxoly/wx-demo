package com.example.demo.utils.sm;

public class SM4Utils
{
	private String secretKey = "FANXIAOXITONGNEW";

	private String iv = "NEWCAPECNEWCAPEC";

	private boolean hexString = false;

	public SM4Utils()
	{
	}

	public void SetSecretKey(String Key)
	{
		this.secretKey = Key;
	}

	public String encryptData_ECB(String plainText)
	{
		try
		{
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = false;
			ctx.mode = SM4.SM4_ENCRYPT;

			byte[] keyBytes;
			if (hexString)
			{
				keyBytes = Util.hexStringToBytes(secretKey);
			}
			else
			{
				keyBytes = secretKey.getBytes();
			}

			SM4 sm4 = new SM4();
			sm4.sm4_setkey_enc(ctx, keyBytes);
			byte[] encrypted = sm4.sm4_crypt_ecb(ctx, Util.hexStringToBytes(plainText));

			String cipherText = Util.encodeHexString(encrypted);
			//String cipherText = new String(encrypted);
			//Log.e("test1",cipherText);
			/*
			String cipherText = new BASE64Encoder().encode(encrypted);
			if (cipherText != null && cipherText.trim().length() > 0)
			{
				Pattern p = Pattern.compile("\\s*|\t|\r|\n");
				Matcher m = p.matcher(cipherText);
				cipherText = m.replaceAll("");
			}*/
			return cipherText;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public String decryptData_ECB(String cipherText)
	{
		try
		{
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = false;
			ctx.mode = SM4.SM4_DECRYPT;

			byte[] keyBytes;
			if (hexString)
			{
				keyBytes = Util.hexStringToBytes(secretKey);
			}
			else
			{
				keyBytes = secretKey.getBytes();
			}

			SM4 sm4 = new SM4();
			sm4.sm4_setkey_dec(ctx, keyBytes);
			byte[] decrypted = sm4.sm4_crypt_ecb(ctx, Util.hexStringToBytes(cipherText));
			return Util.encodeHexString(decrypted);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public String encryptData_CBC(String plainText)
	{
		try
		{
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = true;
			ctx.mode = SM4.SM4_ENCRYPT;

			byte[] keyBytes;
			byte[] ivBytes;
			if (hexString)
			{
				keyBytes = Util.hexStringToBytes(secretKey);
				ivBytes = Util.hexStringToBytes(iv);
			}
			else
			{
				keyBytes = secretKey.getBytes();
				ivBytes = iv.getBytes();
			}

			SM4 sm4 = new SM4();
			sm4.sm4_setkey_enc(ctx, keyBytes);
			byte[] encrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, plainText.getBytes("UTF-8"));
			String cipherText = Util.encodeHexString(encrypted);
			if (cipherText != null && cipherText.trim().length() > 0)
			{
				cipherText = Util.replaceBlank(cipherText);
			}
			return cipherText;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public String decryptData_CBC(String cipherText)
	{
		try
		{
			SM4_Context ctx = new SM4_Context();
			ctx.isPadding = false;
			ctx.mode = SM4.SM4_DECRYPT;

			byte[] keyBytes;
			byte[] ivBytes;
			if (hexString)
			{
				keyBytes = Util.hexStringToBytes(secretKey);
				ivBytes = Util.hexStringToBytes(iv);
			}
			else
			{
				keyBytes = secretKey.getBytes();
				ivBytes = iv.getBytes();
			}

			SM4 sm4 = new SM4();
			sm4.sm4_setkey_dec(ctx, keyBytes);
			// new BASE64Decoder().decodeBuffer(cipherText)
			byte[] decrypted = sm4.sm4_crypt_cbc(ctx, ivBytes, Util.hexStringToBytes(cipherText));
			String deStr = new String(decrypted, "UTF-8");
			return deStr.substring(0, deStr.lastIndexOf("�"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	public static String encode(String data,String key){
		SM4Utils sm4Utils = new SM4Utils();
		sm4Utils.SetSecretKey(key);
		String encryptdata = sm4Utils.encryptData_CBC(data);
		return encryptdata;
	}
	public  static String decode(String data,String key){
		SM4Utils sm4Utils = new SM4Utils();
		sm4Utils.SetSecretKey(key);
		String encryptdata = sm4Utils.decryptData_CBC(data);
		return encryptdata;
	}

}

