package com.example.demo.DTO;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ModifyDTO {
    private String idserial; //学工号
    private String typecode; //码状态类型
    private String  statecode;   //码状态代码

    public JSONObject toJSON(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("idserial",this.idserial);
        jsonObject.put("typecode",this.typecode);
        jsonObject.put("statecode",this.statecode);

        return jsonObject;
    }

    public String toJSONString(){
        return this.toJSON().toJSONString();
    }
}