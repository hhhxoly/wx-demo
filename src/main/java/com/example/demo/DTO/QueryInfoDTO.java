package com.example.demo.DTO;

import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryInfoDTO {
    private String qrcode;
    private String ip;
    private String txdate;
    private String termno;
    private String type;

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("qrcode", this.qrcode);
        jsonObject.put("ip", this.ip);
        jsonObject.put("txdate", this.txdate);
        jsonObject.put("termno", this.termno);
        jsonObject.put("type", this.type);

        return jsonObject;
    }

    public String toJSONString(){
        return this.toJSON().toJSONString();
    }
}