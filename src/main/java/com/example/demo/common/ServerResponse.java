package com.example.demo.common;

import java.io.Serializable;
public class ServerResponse<T> implements Serializable
{
    private int status;
    private String msg;
    private T data;

    private ServerResponse(final int status) {
        this.status = status;
    }

    private ServerResponse(final int status, final T data) {
        this.status = status;
        this.data = data;
    }

    private ServerResponse(final int status, final String msg, final T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    private ServerResponse(final int status, final String msg) {
        this.status = status;
        this.msg = msg;
    }

    public boolean isSuccess() {
        return this.status == ResponseCode.SUCCESS.getCode();
    }

    public int getStatus() {
        return this.status;
    }

    public T getData() {
        return this.data;
    }

    public String getMsg() {
        return this.msg;
    }

    public static <T> ServerResponse<T> createBySuccess() {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode());
    }

    public static <T> ServerResponse<T> createBySuccessMessage(final String msg) {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), msg);
    }

    public static <T> ServerResponse<T> createBySuccess(final T data) {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), data);
    }

    public static <T> ServerResponse<T> createBySuccess(final String msg, final T data) {
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(), msg, data);
    }

    public static <T> ServerResponse<T> createByError() {
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getDesc());
    }

    public static <T> ServerResponse<T> createByErrorMessage(final String errorMessage) {
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(), errorMessage);
    }

    public static <T> ServerResponse<T> createByErrorCodeMessage(final int errorCode, final String errorMessage) {
        return new ServerResponse<T>(errorCode, errorMessage);
    }
}


