package com.example.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.DTO.ModifyDTO;
import com.example.demo.DTO.QueryInfoDTO;
import com.example.demo.DTO.SaveDTO;
import com.example.demo.service.OpenPlatformService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Controller
@RequestMapping("/openPlatform")
@Slf4j
@CrossOrigin
public class OpenPlatformController {

    private static final String platform_url = "http://172.22.128.5/openplartform/business/";
    private static final String modify = "modifySysQrcode";
    private static final String queryInfo = "queryAccountInfoByQrcode";
    private static final String save = "saveSysQrcode";

    private final OpenPlatformService openPlatformService;

    public OpenPlatformController(OpenPlatformService openPlatformService) {
        this.openPlatformService = openPlatformService;
    }

    @PostMapping("/callModifySysQrcode")
    @ResponseBody
    public JSONObject callModifySysQrcode(@RequestBody ModifyDTO modifyDTO){
        log.debug("param: "+modifyDTO.toJSONString());
        return openPlatformService.callOpenPlatform(modifyDTO.toJSON(),platform_url+modify);
    }

    @PostMapping("/callQueryInfo")
    @ResponseBody
    public JSONObject callQueryInfo(@RequestBody QueryInfoDTO queryInfoDTO){
        log.debug("param: "+queryInfoDTO.toJSONString());
        return openPlatformService.callOpenPlatform(queryInfoDTO.toJSON(),platform_url+queryInfo);
    }

    @PostMapping("/callSaveSysQrcode")
    @ResponseBody
    public JSONObject callSaveSysQrcode(@RequestBody SaveDTO saveDTO){
        log.debug("param: "+saveDTO.toJSONString());
        return openPlatformService.callOpenPlatform(saveDTO.toJSON(),platform_url+save);
    }
}
