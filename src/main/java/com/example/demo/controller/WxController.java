package com.example.demo.controller;

import com.example.demo.common.ServerResponse;
import com.example.demo.service.WxService;
import com.example.demo.utils.sm.SM3Digest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/wx")
@CrossOrigin
public class WxController {
    private final WxService wxService;

    public WxController(WxService wxService) {
        this.wxService = wxService;
    }

    @GetMapping("/getToken")
    @ResponseBody
    public ServerResponse getTokenInfo(@RequestParam("url") String currUrl) {
        return wxService.getTokenInfo(currUrl);
    }

    @GetMapping("/encryptToken")
    @ResponseBody
    public String sm3Encrypt(@RequestParam("token") String token){
        return SM3Digest.encode(token);
    }
}

