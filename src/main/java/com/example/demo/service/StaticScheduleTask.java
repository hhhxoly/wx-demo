package com.example.demo.service;

import com.example.demo.utils.MyCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
@Slf4j
public class StaticScheduleTask {
    @Scheduled(cron = "0/60 * * * * ?")
    private void autoClean(){
        log.debug("clean cache");
        MyCache.deleteTimeOut();
    }
}
