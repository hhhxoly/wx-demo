package com.example.demo.service;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.common.ServerResponse;
import com.example.demo.utils.HttpClientHelper;
import com.example.demo.utils.MyCache;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
@ConfigurationProperties(prefix = "wx")
@Setter
public class WxService {
    private String corpid;
    private String corpsecret;
    private String getTokenURL;
    private String noncestr;
    //private String currURL;

    //主要方法
    public ServerResponse getTokenInfo(String currUrl){
        try{
            //currURL = currUrl;
            String tokenStr = getToken();
            String ticket = getTickets(tokenStr);
            String timeStamp = String.valueOf(System.currentTimeMillis()/1000);
            log.debug("url from param: "+currUrl);
            String signature = getSignature(noncestr,ticket,timeStamp, currUrl);
            Map<String,String> data = new HashMap<>();
            data.put("appId", corpid);
            data.put("timestamp",timeStamp);
            data.put("nonceStr", noncestr);
            data.put("signature",signature);
            return ServerResponse.createBySuccess(data);
        }catch (Exception e){
            log.error(e.toString());
            return ServerResponse.createByErrorMessage(e.toString());
        }
    }

    // 获取秘钥
    private String getSignature(String noncestr, String ticket, String timeStamp, String currUrl) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("jsapi_ticket=").append(ticket)
                .append("&noncestr=").append(noncestr)
                .append("&timestamp=").append(timeStamp)
                .append("&url=").append(currUrl);
        return getSha1(stringBuilder.toString());
    }

    // 获取token
    private String getToken(){
        String tokenCache = MyCache.get("wxToken",String.class);
        if(!StringUtils.isEmpty(tokenCache)){
            log.debug("wxToken from cache: "+tokenCache);
            return tokenCache;
        }
        JSONObject param = new JSONObject();
        param.put("corpid", corpid);
        param.put("corpsecret", corpsecret);
        String resultStr = HttpClientHelper.httpGet(getTokenURL,param);
        log.debug("wxToken from api: "+resultStr);

        JSONObject result = JSONObject.parseObject(resultStr);
        String errcode = result.getString("errcode");
        String token = "";
        if("0".equals(errcode)){
            token = result.getString("access_token");
            MyCache.put("wxToken",token,7100*1000L);
        }
        return token;
    }

    // 根据token 获取tickets
    private String getTickets(String token){
        String ticketCache = MyCache.get("apiTicket",String.class);
        if(!StringUtils.isEmpty(ticketCache)){
            log.debug("ticket from cache: "+ticketCache);
            return ticketCache;
        }
        JSONObject param = new JSONObject();
        param.put("access_token",token);
        String resultStr = HttpClientHelper.httpGet("https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket",param);
        log.debug("ticket from api: "+resultStr);

        JSONObject result = JSONObject.parseObject(resultStr);
        String errcode = result.getString("errcode");
        String ticket = "";
        if("0".equals(errcode)){
            ticket = result.getString("ticket");
            MyCache.put("apiTicket",ticket,7100*1000L);
        }
        return ticket;
    }


    // sha1加密
    private static String getSha1(String str) {
        char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f' };
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
            mdTemp.update(str.getBytes("UTF-8"));
            byte[] md = mdTemp.digest();
            int j = md.length;
            char buf[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
                buf[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(buf);
        } catch (Exception e) {
            return null;
        }
    }
}
