package com.example.demo.service;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.utils.MyCache;
import com.example.demo.utils.sm.SM3Digest;
import com.example.demo.utils.sm.SM4Utils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@ConfigurationProperties(prefix = "encrypt")
@Setter
public class OpenPlatformService {

    private String sm4_key;

    public JSONObject callOpenPlatform(JSONObject jsonObject, String url) {
        //1、获取token值
        String token = getDefaultToken();
        log.debug("token: " + token);

        //2、请求刷卡进校接口对参数进行SM4
        String encryptdata = sm4EncryptJSON(jsonObject, sm4_key);
        log.debug("加密结果：" + encryptdata);

        //3、对加密结果进行SM3签名
        String sign = SM3Digest.signEncode(encryptdata);
        log.debug("签名数据:" + sign);
        return restPost(url, token, sm4_key, encryptdata, sign);
    }

    //进一步封装获取token过程
    private String getDefaultToken() {
        String tokenCache = MyCache.get("opToken",String.class);
        if(!StringUtils.isEmpty(tokenCache)){
            log.debug("opToken from cache: "+tokenCache);
            return tokenCache;
        }
        String result = httpqueryOpenPlatToken("http://172.22.128.5/openplartform/auth/api/token");
        JSONObject json = JSONObject.parseObject(result);
        String token = json.getJSONObject("resultData").getString("token");
        if (!StringUtils.isEmpty(token)){
            log.debug("opToken from api: "+token);
            MyCache.put("opToken",token,500*1000L);
        }
        return token;
    }

    //使用sm4加密json
    private String sm4EncryptJSON(JSONObject jsonObject, String key) {
        SM4Utils sm4Utils = new SM4Utils();
        sm4Utils.SetSecretKey(key);
        String encryptdata = sm4Utils.encryptData_CBC(jsonObject.toJSONString());
        return encryptdata;
    }

    //获取开放平台token
    private String httpqueryOpenPlatToken(String url) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.valueOf("application/json; charset=UTF-8"));
        JSONObject jsonObject = new com.alibaba.fastjson.JSONObject();
        jsonObject.put("clientid", "377673220808211939");
        jsonObject.put("secretid", "56ZpxLr5Fom1SBPw15V51v01X96Q5u26");
        HttpEntity<String> requestEntity = new HttpEntity<String>(JSONObject.toJSONString(jsonObject), requestHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        return response.getBody();
    }

    //请求具体接口
    private JSONObject restPost(String url, String openplartformToken, String sm4key, String sm4body, String sign) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        //开放平台请求的token值
        requestHeaders.add("Token", openplartformToken);
        //计算签名
        requestHeaders.add("sign", sign);
        requestHeaders.setContentType(MediaType.valueOf("application/json; charset=UTF-8"));
        HttpEntity<String> requestEntity = new HttpEntity<String>(sm4body, requestHeaders);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        JSONObject jsonObject = JSONObject.parseObject(response.getBody());
        log.debug("api response: " + jsonObject.toJSONString());
        //返回成功失败信息
        String message = jsonObject.getString("message");
        //成功
        if (jsonObject != null && jsonObject.getBoolean("success")) {
            //返回结果数据
            String resultdata = jsonObject.getString("resultData");
            String decoderesultdata = "";
            if (StringUtils.hasText(resultdata)) {
                //国密解密
                decoderesultdata = SM4Utils.decode(resultdata, sm4key);
            }
            jsonObject.put("resultData", decoderesultdata);
        }
        return jsonObject;
    }
}
